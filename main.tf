resource "aws_instance" "inst-01"{
  ami = "ami-048f6ed62451373d9"
  instance_type = var.instance_type             #To call variables
  tags ={
     Name = "terra_client"
     Env = "Test"
       }
      
      availability_zone = "us-east-1b"          #To create instance to this zone
      
}
